//
//  ViewController.m
//  googleMaps
//
//  Created by Clicklabs 104 on 11/3/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"


@interface ViewController (){CLLocationManager *manager;
    
    CLGeocoder *geocoder;
    
    CLPlacemark *placemark;
    
}
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudeValue;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeValue;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *OkButton;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@end

@implementation ViewController
@synthesize latitudeLabel;
@synthesize latitudeValue;
@synthesize longitudeLabel;
@synthesize longitudeValue;
@synthesize addressLabel;
@synthesize OkButton;
@synthesize mapView;
NSString * country;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    manager=[[CLLocationManager alloc]init];
    
    manager.delegate = self;
    
    manager.desiredAccuracy=kCLLocationAccuracyBestForNavigation;
    
    [manager requestAlwaysAuthorization];
    
    geocoder=[[CLGeocoder alloc]init];
    
    placemark=[[CLPlacemark alloc]init];
    
    
    
    //    // Create a GMSCameraPosition that tells the map to display the
    
    //    // coordinate -33.86,151.20 at zoom level 6.
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86 longitude:151.20 zoom:15];
    
    
    
    mapView.myLocationEnabled = YES;
    
    
    
    
    
    // Creates a marker in the center of the map.
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    
    marker.title = @"Sydney";
    
    marker.snippet = @"Australia";
    
    marker.map = mapView;
    
    
    
}

- (IBAction)okButtonAction:(id)sender {

    //[locationManager location];

    [manager startUpdatingLocation];
    }

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation

{
    
    
    
    NSLog(@"didUpdateToLocation:%@",newLocation);
    
    CLLocation *currentLocation=newLocation;
    
    if (currentLocation!=nil)
        
    {
        
        latitudeValue.text=[NSString stringWithFormat:@"%.2f",currentLocation.coordinate.latitude];
        
        longitudeValue.text=[NSString stringWithFormat:@"%.2f",currentLocation.coordinate.longitude];
        
        
        
        CLLocationCoordinate2D position=currentLocation.coordinate;
        
    }
    
    
    
    
    
    // Reverse Geocoding
    
    NSLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        NSLog(@"Found placemarks: %@, error: %@", placemarks,error);
        
        if (error == nil && [placemarks count] > 0) {
            
            // placemark = [placemarks lastObject];
            
            //    NSLog(@"placemark:%@",placemarks);
            
            
            CLPlacemark *placemark = placemarks[0];
            NSDictionary *addressDictionary = placemark.addressDictionary;
            
            NSArray *show= addressDictionary[@"FormattedAddressLines"];
                       country = [show componentsJoinedByString:@"," ] ;
            NSLog(@"%@",country);
            addressLabel.text=[NSString stringWithFormat:@"%@",country];
        }}];
            

        
    
        }
     
    
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
        
        
        
        //            GMSMarker *markerall = [GMSMarker markerWithPosition:newLocation];
        //                        markerall.title = country;
        //                     markerall.map = googlemap;
        //
        //            markerall.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        //
        
        
    
     //                    addressLabel.text = [NSString stringWithFormat:[@"%@ %@\n%@ %@\n%@\n%@" ],
     //
     //                                 placemark.subThoroughfare, placemark.thoroughfare,
     //
     //                                 placemark.postalCode, placemark.locality,
     //
     //                                 placemark.administrativeArea,
     //
     //                                 placemark.country];
     //
     
     
     
     /*   [geocoder reverseGeocodeLocation :nlocation
      
      
      
      completionHandler:^(NSArray *placemarks, NSError *error)
      
      
      
      {
            if (error) {
      
      
      
      NSLog(@"Geocode failed with error: %@", error);
      
      
      
      return;
      
      
      
      }
      
      if (placemarks && placemarks.count > 0)
      
      
      
      {
      
      
      
      //[googlemap clear];
      
      
      
      CLPlacemark *placemark = placemarks[0];
      
            NSDictionary *addressDictionary =
      
      
      
      placemark.addressDictionary;
      
      
      NSArray *show= addressDictionary[@"FormattedAddressLines"];
      
      
      country = [show componentsJoinedByString:@"," ] ;
      
      
      
      NSLog(@"%@",country);
      
      
      
      GMSMarker *markerall = [GMSMarker markerWithPosition:newLocation];
      
      
      
      markerall.title = country;
      
      
      
      markerall.map = googlemap;
      
      
      
      markerall.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
      
      
      
      }*/

@end
