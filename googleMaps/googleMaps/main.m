//
//  main.m
//  googleMaps
//
//  Created by Clicklabs 104 on 11/3/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
